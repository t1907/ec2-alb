resource "aws_lb" "alb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.security_groups
  subnets            = var.subnets

  //  enable_deletion_protection = true

  access_logs {
    bucket  = var.logs_bucket
    prefix  = "alb"
    enabled = true
  }

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "target_group" {
  name                          = "tf-example-lb-tg"
  port                          = 80
  protocol                      = "HTTP"
  vpc_id                        = var.vpc_id
  load_balancing_algorithm_type = "round_robin"

}

resource "aws_lb_target_group_attachment" "target_group" {
  for_each         = var.ec2_instances
  target_group_arn = aws_lb_target_group.target_group.arn
  target_id        = each.key
  port             = 80
}


resource "aws_lb_listener" "alb_target_group_forwarder" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"
  //  ssl_policy        = "ELBSecurityPolicy-2016-08"
  //  certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}
