variable "vpc_id" {}
variable "logs_bucket" {}
variable "subnets" {}
variable "security_groups" {}
variable "ec2_instances" {}