provider "aws" {
  region = "us-east-1"
}

locals {
  ec2s = toset([for i in [1, 2, 3, 4, 5] : "server${i}"])
}

module "s3" {
  source = "./s3"
}

module "key_pair" {
  source = "./kp"
}

module "vpc" {
  source  = "./vpc"
  segment = "test"
  region  = "us-east-1"
}

module "ec2" {
  for_each        = local.ec2s
  source          = "./ec2"
  ec2_name        = each.key
  key_name        = module.key_pair.key_name
  subnets         = module.vpc.vpc_info["subnets"]
  security_groups = [module.vpc.vpc_info["ssh_http"]]
}

locals {
  instances = toset([for m in module.ec2 : m["instance_id"]])
}
module "alb" {
  source          = "./alb"
  logs_bucket     = module.s3.logs_bucket
  security_groups = [module.vpc.vpc_info["ssh_http"]]
  subnets         = module.vpc.vpc_info["subnets"]
  vpc_id          = module.vpc.vpc_info["vpc_id"]
  ec2_instances   = local.instances
}
