resource "aws_s3_bucket" "lb_logs" {
  bucket = "prasad-bucket-for-alb-logs"
}

resource "aws_s3_bucket_public_access_block" "lb_logs_pab" {
  bucket                  = aws_s3_bucket.lb_logs.bucket
  restrict_public_buckets = true
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
}

resource "aws_s3_bucket_policy" "access_log_bucket_policy" {
  bucket = aws_s3_bucket.lb_logs.bucket
  policy = jsonencode(
    {
      Version = "2012-10-17",
      Statement = [
        {
          Sid       = "PublicRead",
          Effect    = "Allow",
          Principal = "*",
          Action    = ["s3:GetObject", "s3:GetObjectVersion", "s3:PutObject"],
          Resource  = ["${aws_s3_bucket.lb_logs.arn}/*"]
        }
      ]
  })
}