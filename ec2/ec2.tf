data "aws_region" "region" {}

locals {
  region  = data.aws_region.region.name
  segment = "test1"
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*amzn2-ami-hvm-2*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.amazon_linux_2.id
  instance_type = "t2.micro"
  key_name      = var.key_name
  tags = {
    Name = var.ec2_name
  }
  subnet_id              = element(tolist(var.subnets), length(var.subnets) - 1)
  vpc_security_group_ids = var.security_groups
  user_data_base64       = filebase64("./bootstrap.sh")
}


