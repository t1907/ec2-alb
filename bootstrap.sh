#! /bin/bash
yum update -y
yum install httpd -y
systemctl start httpd
systemctl status httpd
systemctl enable httpd
echo "<h1>Terraform Instance Launched Successfully on `hostname -f` </h1>" >>  /var/www/html/index.html